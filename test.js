const cryptoRandomString = require('crypto-random-string');
var bcrypt = require("bcrypt")
var jwt = require('jsonwebtoken')
const passport = require("passport")
const myPassport = require('./passport_setup')(passport)
var models = require("./models")
var Sequelize = require("sequelize")
const {isEmpty} = require('lodash')
const nodemailer = require("nodemailer")
const {validateUser, validateInvestor} = require("./validators/signup");
const {validateLogin, validateLoginInvestor} = require("./validators/login")
const {validateChange} = require("./validators/change")
const multer = require('multer');
const path = require('path');
const fs = require('fs'); 
const formidable = require('formidable');
var moment = require("moment")
const cron =  require("node-cron")
const geolib = require('geolib');
const axios = require("axios")
var QRCode = require('qrcode')
const Op = Sequelize.Op;
const sendMail = (email, subject, page) => {
const transporter = nodemailer.createTransport({
        sendmail: true,
    newline: 'unix',
    path: '/usr/sbin/sendmail'
}, {
  from: "No Reply <no-reply@gapaautoparts.com>",
  to: email,
  subject: subject,
});
transporter.sendMail({html: page}, (error, info) => {
     if (error) {
	console.log(error);
  } else {
    console.log('Email sent: ' + info.response);
  }
})
}

/*
models.model.findAll({where: { [Op.or]: [{ [Op.or]: [{ year: 1980}, {year_2: 1980} ]},{ [Op.and]: [{year: {[Op.lt]: 1980}},{ year_2: {[Op.gt]: 1980}}]}] } }).then(models => {
    console.log(models)
})*/
// models.part.findAll().then(parts => {
// parts.map(part => {
// models.model.findOne({where: {id: part.model_id}}).then(model => {
// models.part.update({year: model.year},{where: {id: part.id}}).then(data => console.log(data))
// })
// })
// })

        //   QRCode.toDataURL('https://gapaautoparts.com/order/1323', function (err, url) {
        //                             console.log(url)
        
        //                         })
        
        const geoip = require('fast-geoip');

const ip = "102.91.5.55";
       geoip.lookup(ip).then(geo => {
           console.log(geo)
             var location = geo.region + ', ' + geo.city + ', ' + geo.country
            
            models.search.create({name: null, email: null, phone: null, location: location, ip: ip, search: "test"})
       })